﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentParent : MonoBehaviour
{
    public virtual void UseEquipment()
    {
        Debug.Log("EquipmentParent::UseEquipment()::This function doesn't exist in the " + this.GetType().FullName + " class");
    }
}
