﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    [SerializeField]
    List<EquipmentParent> equipment;
    int equipmentIndex;

    public EquipmentParent GetCurrentEquipment
    {
        get { return equipment[equipmentIndex]; }
    }
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 1; i < equipment.Count; i++)
        {
            equipment[i].gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {//deactivate current equipment and activate the next equipment
            Debug.Log("Switching equipment..." + equipmentIndex);
            equipment[equipmentIndex].gameObject.SetActive(false);
            equipmentIndex++;
            equipmentIndex = equipmentIndex % equipment.Count;
            equipment[equipmentIndex].gameObject.SetActive(true);
        }

        if(Input.GetMouseButtonDown(1))
        {
            GetCurrentEquipment.UseEquipment();
        }
    }
}
