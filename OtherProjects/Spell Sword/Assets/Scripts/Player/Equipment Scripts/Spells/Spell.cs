﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour
{
    [SerializeField]
    float manaCost;
    public float ManaCost
    {
        get { return manaCost; }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<CharacterMovement>() == null)
        {
            if (collision.gameObject.GetComponent<SpellInteractableObject>() != null)
            {
                collision.gameObject.GetComponent<SpellInteractableObject>().OnActivated(this);
            }
        }
    }
}
