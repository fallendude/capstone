﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    #region player stats
    [SerializeField]
    float walkSpeed;
    [SerializeField]
    float runSpeed;
    [SerializeField]
    float turnSpeed;
    #endregion

    #region control adjustments
    [SerializeField][Tooltip("Minimum mouse movement speed to rotate the player.")]
    Vector2 rotationDeadZone;
    #endregion

    #region components
    Rigidbody rigidBody;
    Camera mainCamera;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rigidBody = GetComponent<Rigidbody>();

        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float verticalAngle = mainCamera.transform.eulerAngles.x;
        float horizontalAngle = mainCamera.transform.eulerAngles.y;

        #region direction vectors (based on player rotation)
        Vector3 direction = new Vector3(Mathf.Cos(Mathf.Deg2Rad * verticalAngle) * Mathf.Sin(Mathf.Deg2Rad * horizontalAngle), Mathf.Sin(Mathf.Deg2Rad * verticalAngle), Mathf.Cos(Mathf.Deg2Rad * verticalAngle) * Mathf.Cos(Mathf.Deg2Rad * horizontalAngle));

        //Right vector
        Vector3 right = new Vector3(Mathf.Sin((Mathf.Deg2Rad * horizontalAngle) - 3.14f / 2.0f), 0, Mathf.Cos((Mathf.Deg2Rad * horizontalAngle) - 3.14f / 2.0f));

        //Up vector : perpendicular to both direction and right
        Vector3 up = Vector3.Cross(right, direction);
        #endregion

        Vector3 desiredDirection = (direction * Input.GetAxis("Vertical")) + (right * -Input.GetAxis("Horizontal"));
        #region Make the normalized velocity not affected by vertical velocity
        float tempFallSpeed = rigidBody.velocity.y;
        rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0, rigidBody.velocity.z);
        rigidBody.velocity = desiredDirection.normalized * walkSpeed;
        rigidBody.velocity = new Vector3(rigidBody.velocity.x, tempFallSpeed, rigidBody.velocity.z);
        #endregion

        //Debug.Log(desiredDirection);

        if (desiredDirection.magnitude == 0)
            GetComponent<Rigidbody>().velocity = new Vector3();

        Vector2 desiredRotation = new Vector2(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));
        mainCamera.transform.Rotate(new Vector3(desiredRotation.x, desiredRotation.y, 0) * turnSpeed);

        //limit how low/high the camera can look
        if(mainCamera.transform.eulerAngles.x < 90)
            mainCamera.transform.eulerAngles = new Vector3(Mathf.Clamp(mainCamera.transform.eulerAngles.x, -20, 50), mainCamera.transform.eulerAngles.y, mainCamera.transform.eulerAngles.z);
        else if(mainCamera.transform.eulerAngles.x > 320)
            mainCamera.transform.eulerAngles = new Vector3(Mathf.Clamp(mainCamera.transform.eulerAngles.x, 340, 380), mainCamera.transform.eulerAngles.y, mainCamera.transform.eulerAngles.z);

        //make the camera not tilt
        mainCamera.transform.eulerAngles = new Vector3(mainCamera.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y, 0);

    }
}
