﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField]
    float maxMana;
    float currentMana;
    public float CurrentMana
    {
        get { return currentMana; }
        set { currentMana = value; }
    }
    [SerializeField]
    float manaRegenPerSecond;

    [SerializeField]
    float maxHealth;
    float currentHealth;
    public float CurrentHealth
    {
        get { return currentHealth; }
        set { currentHealth = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<EquipmentManager>().GetCurrentEquipment.GetComponent<BookBehavior>() == null)
        {
            if (currentMana < maxMana)
                currentMana += Time.deltaTime * manaRegenPerSecond;
            Debug.Log("PlayerStats::Update()::currentMana = " + currentMana);
        }
        currentMana = Mathf.Clamp(currentMana, 0, maxMana);
    }

    public void UseMana(float manaCost)
    {
        currentMana -= manaCost;
        Debug.Log("PlayerStats::UseMana()::New mana reserves = " + currentMana);
    }
}
