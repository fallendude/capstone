﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportMe : MonoBehaviour
{
    public GameObject sendMyPlayerHere;

    void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.transform.position = sendMyPlayerHere.transform.position;
        }
    }
}
