﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI_Brawler : MonoBehaviour
{
    //What states does our enemy need to have?
    public enum AIState
    {
        None,
        Patrol,
        Look,
        Pursue,
        Attack,
        Confused
    }

    //Set AI State to null, just precautionary
    public AIState myAIState = AIState.None;

    //Our resident time tracker, could have multiple applications, found in update
    private float SecondsInCurrentState;
    private float waitTime;

    //Our resident Fiend
    private GameObject Jeffery;
    private NavMeshAgent agent;
    public int enemyHealth;

    //All Target GameObjects
    private GameObject playerToKill;
    public List<Transform> patrolRoute;
    private RaycastHit hit;
    
    //All Position variables
    private Vector3 currPos;
    private Vector3 tarPos;
    private Vector3 lastPos;
    private Vector3 targetPosition;
    private Vector3 raycastDir;
    private int destPoint;
    private int routeNum;
    private int oldestRoute;
    public float attackRange;

    void Awake()
    {
        patrolRoute.Clear();
        foreach (GameObject patrolpoint in GameObject.FindGameObjectsWithTag("PatrolPoint"))
        {
            patrolRoute.Add(patrolpoint.transform);
        }
    }

    void Start()
    {
        //Get Jeffery, get his position, get his target (the player)
        Jeffery = gameObject;
        playerToKill = GameObject.FindGameObjectWithTag("Player");
        currPos = Jeffery.transform.position;
        tarPos = playerToKill.transform.position;
        
        //Get the agent information for the nav mesh integration
        agent = Jeffery.GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updatePosition = true;
        agent.autoBraking = false;

        //Set Initial Values
        destPoint = 0;
        routeNum = 0;
        oldestRoute = 0;

        //Set our default initial state
        SetAIState(AIState.Patrol);
    }

    //Patrolling Function
    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (patrolRoute.Count == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = patrolRoute[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % patrolRoute.Count;   
    }

    //Pursuing Function
    void GetThatPlayer()
    {
        //Remember where we last saw the player, different from the player's
        //current position.
        lastPos = tarPos;

        //Set the agent to go to the player
        agent.destination = tarPos;
    }

    void Update()
    {
        if (enemyHealth <= 0)
        {
            Jeffery.SetActive(false);
        }

        Debug.DrawRay(currPos, transform.forward); //REMOVE this later
        SecondsInCurrentState += Time.deltaTime;

        currPos = Jeffery.transform.position;
        tarPos = playerToKill.transform.position;
        targetPosition = agent.destination - currPos;
        targetPosition.y = 0.01f;

        //Look towards where we are walking
        Jeffery.transform.forward = Vector3.RotateTowards(Jeffery.transform.forward, targetPosition, 6.28f * Time.deltaTime, float.PositiveInfinity);

        //Is the player in range of the enemy? Set spotted to false, and check if we can see
        bool spottedHim = false;
        if (Vector3.Distance(currPos, tarPos) < /*This is his hearing range*/5.0f)
        {
            raycastDir = tarPos - currPos;
            
            //Can the enemy reasonably see the player?
            Physics.Raycast(currPos, raycastDir, out hit, 5.0f);
            if(hit.transform != null && hit.transform.tag == "Player")
            {
                //Help us see the sightline when spotted = true, ***REMOVE LATER***
                Debug.DrawRay(currPos, raycastDir);
          
                //Recognize that we have found the player, comes in handy before
                //we return to patrol
                spottedHim = true;
            }
          
        }

        //How do we transition between states? What do we do inside of each state?
        switch (myAIState)
        {
            case AIState.Patrol:
                // Choose the next destination point when the agent gets
                // close to the current one.
                waitTime = Random.Range(1.0f, 3.0f);

                if (!agent.pathPending && agent.remainingDistance < 0.5f)
                {
                    SetAIState(AIState.Look);
                }

                if (spottedHim)
                {
                    SetAIState(AIState.Pursue);
                }

                break;
            case AIState.Look:
                if(SecondsInCurrentState > waitTime)
                {
                    GotoNextPoint();
                    SetAIState(AIState.Patrol);
                }

                if (spottedHim)
                {
                    SetAIState(AIState.Pursue);
                }

                break;
            case AIState.Pursue:
                if(spottedHim)
                {
                    transform.LookAt(playerToKill.transform);
                    GetThatPlayer();
                }
                else
                {
                    agent.destination = lastPos;
                }
                if ((!agent.pathPending && agent.remainingDistance < attackRange) &&
                    spottedHim)
                {
                    SetAIState(AIState.Attack);
                }
                if ((!agent.pathPending && agent.remainingDistance < 0.5f) &&
                    !spottedHim)
                {
                    SetAIState(AIState.Confused);
                }
                break;
            case AIState.Attack:
                if(SecondsInCurrentState >= 1.0f)//animation time
                {
                    SetAIState(AIState.Pursue);
                }
                break;
            case AIState.Confused:
                if(spottedHim)
                {
                    SetAIState(AIState.Pursue);
                }
                else if(SecondsInCurrentState > 1.5f)
                {
                    SetAIState(AIState.Patrol);

                    //Increase the number of our next patrol point
                    routeNum++;
                    int indexSpot = 0;

                    //Create a new patrol point based off of Player's last known location
                    GameObject newPatrolPoint = new GameObject("KnownPlayerPos" + routeNum);
                    newPatrolPoint.transform.position = lastPos;

                    //Check newPatrolPoint against the rest of the patrol points and find the closest
                    Transform tMin = null;
                    float minDist = Mathf.Infinity;
                    foreach (Transform t in patrolRoute)
                    {
                        float dist = Vector3.Distance(t.position, newPatrolPoint.transform.position);
                        if (dist < minDist)
                        {
                            tMin = t;
                            minDist = dist;
                        }
                    }
                    indexSpot = patrolRoute.IndexOf(tMin);
                    indexSpot++;

                    //if closest point is further than 2 meters from the last point, add it to the patrolRoute, otherwise, ignore it
                    if (minDist > 2)
                    {
                        patrolRoute.Insert(indexSpot, newPatrolPoint.transform);
                    }
                    else
                    {
                        Destroy(newPatrolPoint);
                        routeNum--;
                    }

                    //Limit Patrol Size
                    if(patrolRoute.Count > 6)
                    {
                        oldestRoute++;
                        for (int i = 0; i < patrolRoute.Count; i++)
                        {
                            if(patrolRoute[i] == GameObject.Find("KnownPlayerPos" + oldestRoute).transform)
                            {
                                patrolRoute.Remove(GameObject.Find("KnownPlayerPos" + oldestRoute).transform);
                                Destroy(GameObject.Find("KnownPlayerPos" + oldestRoute));
                            }
                        }
                    }
                }
                break;
        }
    }

    //Affect leaving and entering states, and things to do when that happens
    void SetAIState(AIState newState)
    {
        if(newState != myAIState)
        {
            //On Leave State

            myAIState = newState;
            SecondsInCurrentState = 0.0f;

            //On Enter State
            switch(myAIState)
            {
                case AIState.Patrol:
                    break;
                case AIState.Look:
                    break;
                case AIState.Pursue:
                    break;
                case AIState.Attack:
                    Debug.Log("Hey Batter Batter");
                    break;
                case AIState.Confused:
                    agent.destination = currPos;
                    break;
            }
        }
    }
}
