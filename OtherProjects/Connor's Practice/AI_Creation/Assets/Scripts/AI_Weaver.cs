﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI_Weaver : MonoBehaviour
{
    //What states does our enemy need to have?
    public enum AIState
    {
        None,
        ReturnHome,
        Pursue
    }

    //Set AI State to null, just precautionary
    public AIState myAIState = AIState.None;

    //Our resident time tracker, could have multiple applications, found in update
    private float SecondsInCurrentState;
    private float waitTime;

    //Our resident Fiend
    private GameObject Jeffery;
    private NavMeshAgent agent;
    public int enemyHealth;
    public float fireRate;

    //His Attack
    public GameObject spawn;
    public GameObject projectile;

    //All Target GameObjects
    private GameObject playerToKill;
    private RaycastHit hit;

    //All Position variables
    private Vector3 currPos;
    private Vector3 tarPos;
    private Vector3 lastPos;
    private Vector3 vectorToPosition;
    private Vector3 vectorToPlayer;
    private Vector3 raycastDir;
    private GameObject standHere;
    private GameObject lookHere;

    //Enemy Range Values
    public float distanceToTarget;
    private float maxDistance = 5.0f;
    private float minDistance = 3.0f;

    //Start the Encounter by setting this = true
    private bool spottedHim = false;
    private bool needToMove = false;

    void Start()
    {
        //Get Jeffery, get his position, get his target (the player)
        Jeffery = gameObject;
        playerToKill = GameObject.FindGameObjectWithTag("Player");
        currPos = Jeffery.transform.position;
        tarPos = playerToKill.transform.position;
        standHere = GameObject.FindGameObjectWithTag("StandHere");
        lookHere = GameObject.FindGameObjectWithTag("LookHere");

        //Get the agent information for the nav mesh integration
        agent = Jeffery.GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updatePosition = true;
        agent.autoBraking = false;

        //Set our default initial state
        SetAIState(AIState.ReturnHome);
    }

    void Update()
    {
        if(enemyHealth <= 0)
        {
            Jeffery.SetActive(false);
        }

        Debug.DrawRay(currPos, transform.forward); //REMOVE this later
        SecondsInCurrentState += Time.deltaTime;

        currPos = Jeffery.transform.position;
        tarPos = playerToKill.transform.position;
        vectorToPosition = agent.destination - currPos;
        vectorToPlayer = tarPos - currPos;
        distanceToTarget = vectorToPlayer.magnitude;
        vectorToPosition.y = 0.001f;

        //Look towards where we are walking
        Jeffery.transform.forward = Vector3.RotateTowards(Jeffery.transform.forward, vectorToPosition, 6.28f * Time.deltaTime, float.PositiveInfinity);

        if (Vector3.Distance(currPos, tarPos) < 10)
        {
            raycastDir = tarPos - currPos;

            //Can the enemy reasonably see the player?
            Physics.Raycast(currPos, raycastDir, out hit, 5.0f);
            if (hit.transform != null && hit.transform.tag == "Player")
            {
                //Help us see the sightline when spotted = true, ***REMOVE LATER***
                Debug.DrawRay(currPos, raycastDir);

                spottedHim = true;
                lastPos = tarPos;
                needToMove = false;
            }
            else
            {
                needToMove = true;
            }
        }

        //How do we transition between states? What do we do inside of each state?
        switch (myAIState)
        {
            case AIState.ReturnHome:
                agent.destination = standHere.transform.position;
                if (!agent.pathPending && agent.remainingDistance < 0.5f)
                {
                    agent.destination = currPos;
                    SecondsInCurrentState = 0;
                    Jeffery.transform.forward = Vector3.RotateTowards(Jeffery.transform.forward, lookHere.transform.position, 2.1f * Time.deltaTime, float.PositiveInfinity);
                }
                if (spottedHim)
                {
                    SetAIState(AIState.Pursue);
                }
                break;

            case AIState.Pursue:
                transform.LookAt(playerToKill.transform);
                //Set the agent to go to the player
                if (distanceToTarget > maxDistance)
                {
                    agent.destination = tarPos;
                }
                if (distanceToTarget > minDistance && distanceToTarget < maxDistance)
                {
                    agent.destination = currPos;
                    if (needToMove == false)
                    {
                        if (SecondsInCurrentState >= fireRate)
                        {
                            GameObject bullet = Instantiate(projectile, spawn.transform.position, Quaternion.identity) as GameObject;
                            bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 200);
                            SecondsInCurrentState = 0;
                            Destroy(bullet, 3.0f);
                        }
                    }
                    else
                    {
                        agent.destination = Vector3.MoveTowards(currPos, lastPos, float.PositiveInfinity);
                    }
                }
                if (distanceToTarget < minDistance && needToMove == false)
                {
                    agent.destination += -vectorToPlayer;
                    if (SecondsInCurrentState >= (fireRate / 2))
                    {
                        GameObject bullet = Instantiate(projectile, spawn.transform.position, Quaternion.identity) as GameObject;
                        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 200);
                        SecondsInCurrentState = 0;
                        Destroy(bullet, 3.0f);
                    }
                }
                break;
        }
    }

    //Affect leaving and entering states, and things to do when that happens
    void SetAIState(AIState newState)
    {
        if (newState != myAIState)
        {
            //On Leave State

            myAIState = newState;
            SecondsInCurrentState = 0.0f;

            //On Enter State
            switch (myAIState)
            {
                case AIState.ReturnHome:
                    break;
                case AIState.Pursue:
                    break;
            }
        }
    }
}
