﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Sentry : MonoBehaviour
{
    public enum AIState
    {
        None,
        LookLeft,
        LookRight,
        Attack
    }

    //Set AI State to null, just precautionary
    public AIState myAIState = AIState.None;

    //Our resident time tracker, could have multiple applications, found in update
    private float SecondsInCurrentState;

    //All Target GameObjects
    public GameObject playerToKill;
    private RaycastHit hit;
    public Transform[] lookHere;

    //Our direction variables
    private Vector3 toPoint1;
    private Quaternion point1Rotation;
    private Vector3 toPoint2;
    private Quaternion point2Rotation;
    private Vector3 targetDir;

    //Distance variable
    private Vector3 tarPos;

    //Can we see him?
    private bool spottedHim = false;

    // Start is called before the first frame update
    void Start()
    {
        //Set our default initial state
        SetAIState(AIState.LookLeft);

        tarPos = playerToKill.transform.position;

        toPoint1 = lookHere[0].position - gameObject.transform.position;
        toPoint1.y = gameObject.transform.forward.y;
        point1Rotation = Quaternion.Euler(toPoint1);
        toPoint2 = lookHere[1].position - gameObject.transform.position;
        toPoint2.y = gameObject.transform.forward.y;
        point2Rotation = Quaternion.Euler(toPoint2);
    }

    // Update is called once per frame
    void Update()
    {
        tarPos = playerToKill.transform.position;
        targetDir = tarPos - gameObject.transform.position;
        Debug.DrawRay(gameObject.transform.position, transform.forward); //REMOVE this later
        SecondsInCurrentState += Time.deltaTime;
        spottedHim = false;

        float angle1 = Quaternion.Angle(gameObject.transform.rotation, point1Rotation);
        float angle2 = Quaternion.Angle(gameObject.transform.rotation, point2Rotation);

        if (Vector3.Distance(gameObject.transform.position, tarPos) < 5.0f)
        {
            Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, 5.0f);

            if (hit.transform != null && hit.transform.tag == "Player")
            {
                Debug.DrawRay(gameObject.transform.position, gameObject.transform.forward);

                //Recognize that we have found the player, comes in handy before
                spottedHim = true;
            }
        }

        switch (myAIState)
        {
            case AIState.LookLeft:
                gameObject.transform.forward = Vector3.RotateTowards(gameObject.transform.forward, toPoint1, 1.0f * Time.deltaTime, float.PositiveInfinity);
                if (SecondsInCurrentState >= 5)
                {
                    SetAIState(AIState.LookRight);
                }
                if(spottedHim == true)
                {
                    SetAIState(AIState.Attack);
                }
                break;
            case AIState.LookRight:
                gameObject.transform.forward = Vector3.RotateTowards(gameObject.transform.forward, toPoint2, 1.0f * Time.deltaTime, float.PositiveInfinity);
                if(SecondsInCurrentState >= 5)
                {
                    SetAIState(AIState.LookLeft);
                }
                if (spottedHim == true)
                {
                    SetAIState(AIState.Attack);
                }
                break;
            case AIState.Attack:
                gameObject.transform.forward = Vector3.RotateTowards(gameObject.transform.forward, targetDir, 100.0f * Time.deltaTime, float.PositiveInfinity);

                if(angle1 <= 3.0f | angle2 <= 3.0f)
                {
                    Debug.Log("Oh Wow");
                }

                if (spottedHim == false)
                {
                    Debug.Log("Lost him");
                }
                break;
        }
    }

    void SetAIState(AIState newState)
    {
        if (newState != myAIState)
        {
            //On Leave State

            myAIState = newState;
            SecondsInCurrentState = 0.0f;

            //On Enter State
            switch (myAIState)
            {
                case AIState.LookLeft:
                    break;
                case AIState.LookRight:
                    break;
                case AIState.Attack:
                    Debug.Log("Bang Bang into the Room");
                    break;
            }
        }
    }
}
