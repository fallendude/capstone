﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMore : MonoBehaviour
{
    public GameObject MeleeMan;

    private int numEnemies = 0;
    public int spawnDelay;

    private float timeSinceLastSpawn;

    private GameObject Spawner;
    void Start()
    {
        Spawner = GameObject.FindGameObjectWithTag("Spawner");
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");

        numEnemies = Enemies.Length;
        timeSinceLastSpawn += Time.deltaTime;
        
        if(numEnemies <= 4 && timeSinceLastSpawn >= spawnDelay)
        {
            Instantiate(MeleeMan, Spawner.transform.position, Quaternion.identity);
            timeSinceLastSpawn = 0;
        }
    }
}
